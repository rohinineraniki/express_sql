import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Employee from "./components/Employee";
import Home from "./components/Home";
import Departments from "./components/Departments";

import DeptGender from "./components/DeptGender";
import Salaries from "./components/Salaries";
import HireCount from "./components/HireCount";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route
          exact
          path="/employees-by-departments"
          element={<Departments />}
        />
        <Route
          exact
          path="/employees-by-department-gender"
          element={<DeptGender />}
        />
        <Route exact path="/department-salary" element={<Salaries />} />
        <Route
          exact
          path="/employee-current-dept-manager"
          element={<Employee />}
        />
        <Route
          exact
          path="/employee-hire-last-n-months"
          element={<HireCount />}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
