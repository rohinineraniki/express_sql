import React from "react";
import { Link } from "react-router-dom";

import "./Home.css";

function Home(props) {
  return (
    <div className="m-5">
      <Link to="/" className="link">
        <p className="info">Home Page</p>
        <p className="info">
          Click below link to get the total number of employees for a given
          department
        </p>
        <Link to="/employees-by-departments">
          <p>employees-by-departments</p>
        </Link>

        <p className="info">
          Click below link to get the total number of employees of a given
          gender for a given department
        </p>
        <Link to="/employees-by-department-gender">
          <p>employees-by-department-gender</p>
        </Link>

        <p className="info">
          Click below link to get total salary for a selected department
        </p>
        <Link to="/department-salary">
          <p>department-salary</p>
        </Link>

        <p className="info">
          Click below link to get list of all employees with their manager and
          current working department
        </p>
        <Link to="/employee-current-dept-manager">
          <p>employee-current-dept-manager</p>
        </Link>

        <p className="info">
          Click below link to get number of employees hired in last N months
        </p>
        <Link to="/employee-hire-last-n-months">
          <p>employee-hire-last-n-months</p>
        </Link>
      </Link>
    </div>
  );
}

export default Home;
