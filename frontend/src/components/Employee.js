import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

function Employee(props) {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      console.log("in useffect");
      try {
        const response = await axios.get("/employee-current-dept-manager");
        console.log("response.data", response.data);
        // setData(response.data);
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, []);

  return (
    <div>
      <Link to="/">Home</Link>
      <h1>Employee details</h1>
    </div>
  );
}

export default Employee;
