import React, { Component } from "react";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

class HireChart extends Component {
  static demoUrl =
    "https://codesandbox.io/s/scatter-chart-with-joint-line-2ucid";

  render() {
    const { total_count, months } = this.props;

    const data = [{ x: months, y: total_count }];

    return (
      <ResponsiveContainer width="100%" height={400}>
        <ScatterChart
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" dataKey="x" name="months" />
          <YAxis type="number" dataKey="y" name="employees" />
          <ZAxis type="number" range={[100]} />
          <Tooltip cursor={{ strokeDasharray: "3 3" }} />
          <Legend />

          <Scatter
            name="Number of employees hired"
            data={data}
            fill="#4f0a2a"
            line
            shape="diamond"
            lineJointType="monotoneX"
          />
        </ScatterChart>
      </ResponsiveContainer>
    );
  }
}

export default HireChart;
