import React, { useState, useEffect } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";

import { all_departments } from "./AllDepartments";
import DeptPiechart from "./charts/DeptPiechart";

function Salaries(props) {
  const [deptName, setDeptName] = useState("");
  const [result, setResult] = useState([]);

  const { search, pathname } = useLocation();
  const params = new URLSearchParams(search);
  const navigate = useNavigate();

  const submitBtn = (event) => {
    event.preventDefault();
    params.set("department", deptName);

    navigate({
      pathname: pathname,
      search: params.toString(),
    });
  };

  const changeCategory = (event) => {
    setDeptName(event.target.value);
  };

  useEffect(() => {
    const searchDept = params.get("department");
    console.log("effe", searchDept);

    const fetchData = async () => {
      try {
        const response = await axios.get(
          `/department-salary?department=${searchDept}`
        );
        console.log("response.data", response.data.output);

        setResult(response.data.output);
      } catch (err) {
        console.log(err);
      }
    };

    if (searchDept) {
      fetchData();
    }
  }, [search.toString()]);

  return (
    <div className="m-4">
      <Link to="/">Home</Link>
      <div className="mt-3">
        <h5>Select department to know total salary of that department</h5>

        <form onSubmit={submitBtn}>
          <select onClick={changeCategory} className="form-select w-25">
            {all_departments.map((each) => {
              return (
                <option key={each} value={each.toLowerCase()}>
                  {each}
                </option>
              );
            })}
          </select>
          <button className="btn btn-success mt-3">Submit</button>
        </form>
      </div>
      <div className="mt-3">
        {result.length !== 0 && (
          <>
            <h5>
              Total salary of {result[0].dept_name} department is Rs.
              {result[0].total_salary}
            </h5>
          </>
        )}
      </div>
    </div>
  );
}
export default Salaries;
