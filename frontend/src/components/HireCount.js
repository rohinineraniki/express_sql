import React, { useState, useEffect } from "react";
import { useLocation, useNavigate, Link } from "react-router-dom";
import axios from "axios";
import HireChart from "./charts/HireChart";

function HireCount() {
  const [months, setMonths] = useState(0);
  const [result, setResult] = useState([]);

  const { search, pathname } = useLocation();
  const params = new URLSearchParams(search);
  const navigate = useNavigate();

  const submitBtn = (event) => {
    event.preventDefault();
    params.set("months", months);

    navigate({
      pathname: pathname,
      search: params.toString(),
    });
  };

  const changeMonth = (event) => {
    setMonths(event.target.value);
  };

  useEffect(() => {
    const searchMonths = params.get("months");

    const fetchData = async () => {
      try {
        const response = await axios.get(
          `/employee-hire-last-n-months?months=${searchMonths}`
        );
        console.log("response.data", response.data.output);

        setResult(response.data.output);
      } catch (err) {
        console.log(err);
      }
    };

    if (searchMonths) {
      fetchData();
    }
  }, [search.toString()]);

  return (
    <div className="m-3">
      <Link to="/">Home</Link>
      <p>
        Enter number of monthes to know how many employees hired in that months
      </p>
      <form onSubmit={submitBtn}>
        <input
          type="number"
          className="form-control w-50"
          required
          onChange={changeMonth}
          min="270"
          max="500"
          placeholder="Enter number of months  between 270 to 500 "
        />
        <button className="btn btn-success mt-3">Submit</button>
      </form>
      <div className="mt-4">
        {result.length !== 0 && (
          <>
            <h5>No of employees hired is {result[0].no_of_employees_hired}</h5>
            <HireChart
              total_count={result[0].no_of_employees_hired}
              months={months}
            />
          </>
        )}
      </div>
    </div>
  );
}

export default HireCount;
