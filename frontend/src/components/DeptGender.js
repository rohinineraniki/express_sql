import React, { useState, useEffect } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";

import axios from "axios";
import { all_departments } from "./AllDepartments";
import DeptGenderChart from "./charts/DeptGenderChart";
import DeptPiechart from "./charts/DeptPiechart";

function DeptGender(props) {
  const [parameters, setParameters] = useState({
    deptName: "customer service",
    gender: "M",
  });
  const [result, setResult] = useState([]);

  const { search, pathname } = useLocation();
  const navigate = useNavigate();
  const params = new URLSearchParams(search);

  const searchDept = params.get("department");
  const searchGender = params.get("gender");

  const changeCategory = (event) => {
    setParameters({ ...parameters, deptName: event.target.value });
  };

  const changeGender = (event) => {
    console.log(event.target.value.slice(0, 1));
    setParameters({ ...parameters, gender: event.target.value.slice(0, 1) });
  };

  const submitBtn = (event) => {
    event.preventDefault();
    params.set("department", parameters.deptName);
    params.set("gender", parameters.gender);
    navigate({
      pathname: pathname,
      search: params.toString(),
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `/employees-by-department-gender?gender=${searchGender}&department=${searchDept}`
        );
        console.log("parameters", parameters, searchDept, searchGender);
        console.log("response.data", response);
        setResult(response.data.output);
      } catch (err) {
        console.log(err);
      }
    };
    if (searchDept && searchGender) {
      console.log("if in");
      fetchData();
    }
  }, [searchGender, searchDept]);

  return (
    <div className="ms-3">
      <Link to="/">Home</Link>
      <div className="mt-3">
        <h5>Select department and gender to know total employees</h5>

        <form onSubmit={submitBtn} gap="3">
          <select onClick={changeCategory} className="form-select w-25 mt-2">
            {all_departments.map((each) => {
              return <option key={each}>{each}</option>;
            })}
          </select>

          <select onClick={changeGender} className="form-select w-25 mt-2">
            <option>Male</option>
            <option>Female</option>
          </select>

          <button className="btn btn-success mt-2">Submit</button>
        </form>
      </div>
      <div className="mt-3">
        {console.log(result)}
        {result.length !== 0 && (
          <>
            <h5>
              {result[0].total_no_of_employees}{" "}
              {result[0].gender === "F" ? "female" : "male"} employees are
              working in {result[0].dept_name}
            </h5>
            <DeptGenderChart chartValues={result[0]} />
            {/* <DeptPiechart /> */}
          </>
        )}
      </div>
    </div>
  );
}
export default DeptGender;
