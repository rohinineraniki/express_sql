import React, { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import { all_departments } from "./AllDepartments";
import DeptPiechart from "./charts/DeptPiechart";

function Departments(props) {
  const [deptName, setDeptName] = useState("");
  const [result, setResult] = useState([]);

  const { search, pathname } = useLocation();
  const params = new URLSearchParams(search);
  const navigate = useNavigate();

  const changeCategory = (event) => {
    setDeptName(event.target.value);
  };

  const submitBtn = (event) => {
    event.preventDefault();

    params.set("department", deptName);
    navigate({
      pathname: pathname,
      search: params.toString(),
    });
  };

  useEffect(() => {
    const searchDept = params.get("department");
    console.log("effe", searchDept);

    const fetchData = async () => {
      try {
        const response = await axios.get(
          `/employees-by-departments?department=${searchDept}`
        );
        console.log("response.data", response.data.output);

        setResult(response.data.output);
      } catch (err) {
        console.log(err);
      }
    };

    if (searchDept) {
      fetchData();
    }
  }, [search.toString()]);

  return (
    <div className="ms-3">
      <Link to="/">Home</Link>
      <div className="mt-3">
        <h5>Select department to know total employees</h5>

        <form onSubmit={submitBtn}>
          <select onClick={changeCategory} className="form-select w-25">
            {all_departments.map((each) => {
              return (
                <option key={each} value={each.toLowerCase()}>
                  {each}
                </option>
              );
            })}
          </select>
          <button className="btn btn-success mt-3">Submit</button>
        </form>
      </div>

      <div className="mt-3">
        {result.length !== 0 && (
          <>
            <h5>
              In {result[0].dept_name} department{" "}
              {result[0].total_no_of_employees} people are working
            </h5>
            <DeptPiechart
              department={result[0].dept_name}
              total={result[0].total_no_of_employees}
            />
          </>
        )}
      </div>
    </div>
  );
}

export default Departments;
