import mysql from "mysql";
import dotenv from "dotenv";

dotenv.config();

const database = mysql.createPool(process.env.DATABASE_URL);

database.getConnection((err) => {
  if (err) {
    console.error("error in connecting: ", err);
  } else {
    console.log("connected to database");
  }
});

export default database;
