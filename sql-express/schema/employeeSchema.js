import { query } from "express-validator";
console.log("emp-schema");

const employeeSchema = [
  query("department")
    .isAlpha("en-IN", { ignore: " " })
    .withMessage("enter alpha"),

  // query().custom((value, { req }) => {
  //   const all_departments = [
  //     "customer service",
  //     "development",
  //     "Finance",
  //     "Human Resources",
  //     "Marketing",
  //     "Production",
  //     "Quality Management",
  //     "Research",
  //     "Sales",
  //   ];

  //   console.log("check", query("department"));
  //   if (query("department").isIn(all_departments)) {
  //     return true;
  //   } else {
  //     throw new Error("Invalid department name");
  //   }
  // }),
];

export default employeeSchema;
