function showError(status, message) {
  return {
    status: status,
    message: message,
  };
}

export default showError;
