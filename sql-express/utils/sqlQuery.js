import database from "../connection/database.js";

function sqlQuery(sql, post) {
  return new Promise((resolve, reject) => {
    database.query(sql, post, (err, result) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
}

export default sqlQuery;
