import sqlQuery from "../utils/sqlQuery.js";

export async function byDepartment(request, response, next) {
  const department = request.query.department;
  console.log("dept", department);
  const getEmployeesQuery = `CALL employees_by_dept (?)`;

  try {
    const getEmployeesByDept = await sqlQuery(getEmployeesQuery, department);

    response.json({
      message: "Employees from given department",
      output: getEmployeesByDept[0],
    });
  } catch (err) {
    console.error(err);
    next(err);
  }
}

export async function byDeptGender(request, response, next) {
  const { department, gender } = request.query;

  const getEmployeesQuery = `CALL employees_by_dept_and_gender(?,?)`;
  const queryParams = [gender, department];

  try {
    const getEmployees = await sqlQuery(getEmployeesQuery, queryParams);
    response.json({
      message: "Employees from given department and gender",
      output: getEmployees[0],
    });
  } catch (err) {
    console.log(err);
    next(err);
  }
}

export async function salaryByDept(request, response, next) {
  const { department } = request.query;

  const getSalaryQuery = `CALL total_salary_by_dept(?)`;

  try {
    const getSalary = await sqlQuery(getSalaryQuery, department);
    response.json({
      message: "Total salary of given department",
      output: getSalary[0],
    });
  } catch (err) {
    console.error(err);
    next(err);
  }
}

export async function currentDeptManager(request, response, next) {
  const getDetailsQuery = `CALL employee_current_dept_manager()`;

  try {
    const getDetails = await sqlQuery(getDetailsQuery);
    response.json({
      message: "Employee current working department and respective manager",
      output: getDetails[0],
    });
  } catch (err) {
    console.log(err);
    next(err);
  }
}

export async function hireCount(request, response, next) {
  const { months } = request.query;

  const getHireCountQuery = `CALL employees_hired_in_N_months(?)`;

  try {
    const getHireCount = await sqlQuery(getHireCountQuery, months);
    response.json({
      message: `Employees hired in last ${months} months`,
      output: getHireCount[0],
    });
  } catch (err) {
    console.error(err);
    next(err);
  }
}
