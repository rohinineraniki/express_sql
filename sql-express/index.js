import express from "express";
import CreateError from "http-errors";

import router from "./routes/router.js";

const app = express();
const PORT = process.env.PORT || 8000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/", router);

app.use((req, res, next) => {
  next(CreateError(404));
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({ message: err.message || "Error" });
});

app.listen(PORT, () => {
  console.log(`Server running on  ${PORT}`);
});
