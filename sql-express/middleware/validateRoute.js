import { validationResult } from "express-validator";
import showError from "../utils/showError.js";

function validateRoute(request, response, next) {
  //console.log(validationResult(request));

  if (validationResult(request).errors.length > 0) {
    next(showError(400, "Enter valid department"));
  } else {
    next();
  }
}

export default validateRoute;
