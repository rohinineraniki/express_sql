import express from "express";

import {
  byDepartment,
  byDeptGender,
  salaryByDept,
  currentDeptManager,
  hireCount,
} from "../controllers/getEmployees.js";

import validateRoute from "../middleware/validateRoute.js";
import employeeSchema from "../schema/employeeSchema.js";

const router = express.Router();

router.get("/", (req, res, next) => {
  res.json({
    valid_urls: [
      "/employees-by-departments?department=",
      "/employees-by-department-gender?department=,gender=",
      "/department-salary?department=",
      "/employee-current-dept-manager",
      "/employee-hire-last-n-months?months=",
    ],
  });
});

router.get(
  "/employees-by-departments",
  employeeSchema,
  validateRoute,
  byDepartment
);

router.get("/employees-by-department-gender", byDeptGender);

router.get("/department-salary", salaryByDept);

router.get("/employee-current-dept-manager", currentDeptManager);

router.get("/employee-hire-last-n-months", hireCount);

export default router;
